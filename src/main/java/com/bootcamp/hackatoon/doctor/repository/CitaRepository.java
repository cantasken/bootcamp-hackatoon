package com.bootcamp.hackatoon.doctor.repository;

import com.bootcamp.hackatoon.doctor.model.Cita;
import com.bootcamp.hackatoon.doctor.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitaRepository extends JpaRepository<Cita,Integer> {

}
