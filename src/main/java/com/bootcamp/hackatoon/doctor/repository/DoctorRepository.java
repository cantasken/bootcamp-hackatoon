package com.bootcamp.hackatoon.doctor.repository;

import com.bootcamp.hackatoon.doctor.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor,Integer> {
}
