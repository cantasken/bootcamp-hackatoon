package com.bootcamp.hackatoon.doctor.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name="cita", schema = "equipo4")
public class Cita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String cliente;
    @Column(name = "iddoctor")
    private String id_Doctor;
    private String fecha;
    private String hora;
    private String estado;

}
