package com.bootcamp.hackatoon.doctor.controller;

import com.bootcamp.hackatoon.doctor.model.Doctor;
import com.bootcamp.hackatoon.doctor.service.DoctorService;
import com.bootcamp.hackatoon.doctor.service.impl.DoctorServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctor-online/v1/hackatoon/")
public class DoctorController {

	@Autowired
	private DoctorService service;

	@GetMapping("listar-doctor")
	public @ResponseBody
	 List<Doctor> listarDoctor(){
		return service.listarDoctores();
	}

	@GetMapping("detail-doctor/{id}")
	public @ResponseBody
	 Doctor detailDoctor(@PathVariable("id") Integer id){
		return service.detalleDoctor(id);
	}

}
