package com.bootcamp.hackatoon.doctor.controller;

import com.bootcamp.hackatoon.doctor.dto.CitaDTO;
import com.bootcamp.hackatoon.doctor.model.Cita;
import com.bootcamp.hackatoon.doctor.service.CitaService;
import com.bootcamp.hackatoon.doctor.service.impl.DoctorServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctor-online/v1/hackatoon/")
public class CitaController {

    @Autowired
    private CitaService service;

    @PostMapping("grabar-cita")
    public @ResponseBody
    Cita grabarCita(@RequestBody CitaDTO cita){
        return service.grabarCita(cita);
    }

    @GetMapping("listar-cita")
    public  @ResponseBody
    List<Cita> listarCitas(){
        return service.listarCita();
    }

}
