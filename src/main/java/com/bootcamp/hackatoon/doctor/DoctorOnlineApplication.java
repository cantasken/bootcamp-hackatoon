package com.bootcamp.hackatoon.doctor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient*/
@SpringBootApplication
public class DoctorOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctorOnlineApplication.class, args);
	}

}
