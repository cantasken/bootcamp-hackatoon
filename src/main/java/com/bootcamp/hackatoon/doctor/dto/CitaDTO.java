package com.bootcamp.hackatoon.doctor.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CitaDTO {

    private Integer id;
    private String cliente;
    private String idDoctor;
    private String fecha;
    private String hora;
    private String estado;

}
