package com.bootcamp.hackatoon.doctor.dto;

import java.util.List;

public class DoctorDTO {

    private Integer id;
    private String name;
    private String email;
    private String redessociales;
    private String distancia;
    private String estrellas;
    private String colegiado;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getRedessociales() {
        return redessociales;
    }

    public String getDistancia() {
        return distancia;
    }

    public String getEstrellas() {
        return estrellas;
    }

    public String getColegiado() {
        return colegiado;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRedessociales(String redessociales) {
        this.redessociales = redessociales;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public void setEstrellas(String estrellas) {
        this.estrellas = estrellas;
    }

    public void setColegiado(String colegiado) {
        this.colegiado = colegiado;
    }
}


