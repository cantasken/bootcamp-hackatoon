package com.bootcamp.hackatoon.doctor.service;

import com.bootcamp.hackatoon.doctor.dto.CitaDTO;
import com.bootcamp.hackatoon.doctor.model.Cita;

import java.util.List;

public interface CitaService {

    Cita grabarCita(CitaDTO cita);
    List<Cita> listarCita();
}
