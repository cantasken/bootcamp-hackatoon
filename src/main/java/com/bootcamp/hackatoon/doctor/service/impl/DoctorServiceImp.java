package com.bootcamp.hackatoon.doctor.service.impl;

import com.bootcamp.hackatoon.doctor.model.Doctor;
import com.bootcamp.hackatoon.doctor.repository.DoctorRepository;
import com.bootcamp.hackatoon.doctor.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorServiceImp implements DoctorService {

    @Autowired
    private DoctorRepository repo;


    @Override
    public List<Doctor>  listarDoctores() {
        return  repo.findAll();
    }

    @Override
    public Doctor detalleDoctor(Integer id) {
        if(id != null) {
            if( repo.findById(id).stream().count() > 0 )
                return repo.findById(id).get();
            else
                return new Doctor();
        }
        else
            return new Doctor();
    }
}
