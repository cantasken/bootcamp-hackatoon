package com.bootcamp.hackatoon.doctor.service.impl;

import com.bootcamp.hackatoon.doctor.dto.CitaDTO;
import com.bootcamp.hackatoon.doctor.model.Cita;
import com.bootcamp.hackatoon.doctor.repository.CitaRepository;
import com.bootcamp.hackatoon.doctor.service.CitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CitaServiceImp implements CitaService {

    @Autowired
    private CitaRepository repo;

    @Override
    public Cita grabarCita(CitaDTO c) {
        Cita cita = new Cita();

        cita.setCliente(c.getCliente());
        cita.setEstado(c.getEstado());
        cita.setFecha(c.getFecha());
        cita.setHora(c.getHora());
        cita.setId_Doctor(c.getIdDoctor());

        return repo.save(cita);

    }

    @Override
    public List<Cita> listarCita() {
        return repo.findAll();
    }

}
