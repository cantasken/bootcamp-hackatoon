package com.bootcamp.hackatoon.doctor.service;

import com.bootcamp.hackatoon.doctor.model.Doctor;

import java.util.List;

public interface DoctorService {

    List<Doctor> listarDoctores();
    Doctor detalleDoctor(Integer id);

}
